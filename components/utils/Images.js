export const icons = {
  email: require('../icons/email.png'),
  user: require('../icons/user.png'),
  call: require('../icons/call.png'),
  menu: require('../icons/menu.png'),
  key: require('../icons/key.png'),
};
