/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import {
  ToastAndroid, Switch,
  Platform,View,StyleSheet,TouchableOpacity, Image,Text,
} from 'react-native';

 export const notifyMessage = msg => {
  if (Platform.OS === 'android') {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  } else {
    alert(msg);
  }
};

export const SwitchExample = (props) => {
  return (
     <View style = {styles.container}>
        <Switch
        onValueChange = {props.toggleSwitch1}
        value = {props.switch1Value}/>
     </View>
  );
};

export function MyToolbar  (props) {
  return (
      <View style={{flexDirection: 'row', height: 60, backgroundColor: 'white' , elevation: 10,  shadowOpacity: 0.30, shadowRadius: 6,padding: 10}}>
       <TouchableOpacity style={{flex: 0.5, alignSelf: 'center'}}
               onPress={() => props.navigation.goBack() } >
            <Image style={{width: 25, height: 25, alignSelf: 'flex-start'} } source={url} />
           </TouchableOpacity>

        <View  style={{flex: 2, alignSelf: 'center'}}>
            <Text style={{color: 'black', fontWeight: 'bold', fontStyle: 'italic',marginHorizontal: 10}}>{props.title}</Text>
        </View>
       </View>
  );
};

const styles = StyleSheet.create({
  container: {
     flex: 1,
     padding: 10,
  },
});

// export {
//   notifyMessage, SwitchExample,MyToolbar,
// };
