const Constant = {
  url: 'https://jsonplaceholder.typicode.com/todos/1',
  success: 'Success',
  failure: 'Failure',
  name: 'name',
  pass: '000',

  COUNTRY_DATA: 'COUNTRY_DATA',
  DATA_ERROR: 'DATA_ERROR',
  DATA_SUCCESS: 'DATA_SUCCESS',

  TASK_DATA: 'TASK_DATA',
  TASK_DATA_ERROR: 'TASK_DATA_ERROR',
  TASK_DATA_SUCCESS: 'TASK_DATA_SUCCESS',

  LOGIN_DATA: 'LOGIN_DATA',
  LOGIN_DATA_SUCCESS: 'LOGIN_DATA_SUCCESS',
  LOGIN_DATA_ERROR: 'LOGIN_DATA_ERROR',

  TOKEN_DATA: 'TOKEN_DATA',
  TOKEN_DATA_SUCCESS: 'TOKEN_DATA_SUCCESS',
  TOKEN_DATA_ERROR: 'TOKEN_DATA_ERROR',

  user: 'user',
  emp_name: 'emp_name',
  emp_email: 'emp_email',
  mobile: 'mobile',
  designation: 'designation',
  timestamp: 'timestamp',
  user_type: 'user_type',
  profile_pic: 'profile_pic',
  access_token: 'access_token',
  department: 'department',
};

export {Constant};
