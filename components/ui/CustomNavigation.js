/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-lone-blocks */
/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
  import {
    Image,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Platform,
  } from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import GenerateToken from '../ui/GenerateToken';


let url = {uri: 'https://reactjs.org/logo-og.png'};
// import {NavigationActions} from 'react-navigation';
// const resetAction = NavigationActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Splash'}),
//   ] });
import { useNavigation } from '@react-navigation/native';

class CustomNavigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
          profile_pic:'https://reactjs.org/logo-og.png',
        };
         // Get Read Data from DB
      this.readData().then((data) => {
        console.log('Read Data' + data);
    });
      }

      navigation = {useNavigation}

      clearAsyncStorage = async ()=>{
        try {
          AsyncStorage.clear();
          await  AsyncStorage.setItem('isLogin', 'false');
          this.props.navigation.navigate('Login');
          // useNavigation.navigate('Task');
          // let user = await AsyncStorage.getItem('isLogin');
        }
        catch (error){
          await  AsyncStorage.setItem('isLogin', 'false');
          // this.props.navigation.dispatch(resetAction);
          // useNavigation.navigate('Task');
          alert(error);
        }
      }

      readData = async () => {
        try {
         let profile_pic = await AsyncStorage.getItem('profile_pic');
         console.log('profile_pic => ' + profile_pic);
         this.setState({profile_pic: profile_pic});
        } catch (error) {
          alert(error);
        }
      }

  render() {
    console.log('CustomNavigation');
    return (
     <View>

<View style = {[{marginTop: 20}]}>
    <Image style={[styles.viewHW, styles.imageMargin, {borderRadius: 50 / 2, borderColor: 'green',  borderWidth:1}]} source={{uri: this.state.profile_pic}} />
    <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 5}}>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#D16223'}}>N </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#489DC7'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#E8C24C'}}>R </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'green'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'orange'}}>S</Text>
</View></View>

<View style={{marginStart: 15}}>
<TouchableOpacity
            onPress={() =>  this.props.navigation.dispatch(DrawerActions.closeDrawer())}
            style={[{ marginTop: 20, marginBottom: 10, backgroundColor: 'white'}]}>
            <Text
              style={{
                color: 'black',
                fontSize: 14,
                fontWeight: 'normal',
              }}>
             Home
            </Text>
          </TouchableOpacity>
          <View style={{width: '100%',marginTop: 10, marginBottom: 10, height: 1, backgroundColor: '#f5f5f5'}} />
          <TouchableOpacity
          onPress={() =>  this.clearAsyncStorage()}
            style={[{  marginBottom: 10, backgroundColor: 'white'}]}>
          <Text
              style={{
                color: 'black',
                fontSize: 14,
                fontWeight: 'normal',
              }}>
             Logout
            </Text>
          </TouchableOpacity>

          </View>
     </View>
        );
  }
}
const styles = StyleSheet.create({
    viewHeight: {
      height: 40,
    },
    viewHW: {
      height: 50,
      width: 50,
      alignSelf: 'center',
    },
    imageMargin: {
      ...Platform.select({ios: {marginStart: 0}, android: {marginStart: 0}}),
    },
    container: {
      margin: 0,
      padding: 0,
      borderRadius: 0,
      ...Platform.select({
        ios: {backgroundColor: 'white'},
        android: {backgroundColor: 'white'},
      }),
      // justifyContent:'center'
      // alignSelf: 'center'
    },
    button: {
      justifyContent: 'center',
      height: 40,
      marginLeft: 15,
      marginEnd: 15,
      borderRadius: 5,
      ...Platform.select({
        ios: {backgroundColor: 'green'},
        android: {backgroundColor: 'green'},
      }),
    },
    margin: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
    },
    marginHorizontal: {marginHorizontal: 10},
    marginStart_10: {marginStart: 10},
    marginTop_20: {marginTop: 20},
    marginEnd: {marginEnd: 10},
    marginBottom: {marginBottom: 10},
  
    flexRowDirect: {flexDirection: 'row'},
  
    paragraph: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    paragraphCenter: {
      textAlign: 'center',
    },
    border: {
      borderWidth: 2,
      borderColor: '#7397F5',
      borderRadius: 6,
      ...Platform.select({
        ios: {padding: 10},
        android: {padding: 0},
      }),
    },
    separator: {
      marginVertical: 8,
      borderBottomColor: '#737373',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    linearGradient: {
      flex: 1,
      paddingLeft: 1,
      paddingRight: 1,
      borderRadius: 0,
    },
    borderInner: {
      shadowColor: '#000',
      shadowOpacity: 0.30,
      shadowRadius: 6,
      elevation:5,
      borderWidth: 0,
      borderColor: '#7397F5',
      padding: 15,
      marginTop: 15,
      borderRadius: 6,
      ...Platform.select({
        ios: { backgroundColor: 'white'},
        android: { backgroundColor: 'white'},
      }),
    },
  });
export default CustomNavigation;
