/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, {Component} from 'react';
 import {
   SafeAreaView,
   ScrollView,
   Text,
   View,
   Platform,
   StyleSheet,
   ImageBackground,
   Button,
 } from 'react-native';
 import {Provider} from 'react-redux';
 import Login from './components/ui/Login';
 import MyList from './components/ui/MyList';
 import {Constant} from './components/utils/Constant';
 import configurestore from './components/api/store/store';
 import Navigation from './components/Navgraph/Navigation';
 
 const url = {uri: 'https://reactjs.org/logo-og.png'};
 
 const store = configurestore();
 
 class App extends Component {
   constructor() {
     super();
     this.state = {
       name: Constant.name,
     };
   }
   render() {
     console.log(Platform.OS);
     return (
      // eslint-disable-next-line-react-native/no-inline-styles
       <SafeAreaView style={{flex: 1}}>
         <ImageBackground source={url} style={{flex: 1}}>
           {/* <Provider store={store}>
             <Login username={this.state.name} />
           </Provider> */}
         </ImageBackground>
       </SafeAreaView>
       // <Navigation />
     );
   }
 }
 
 // eslint-disable-next-line no-lone-blocks
 {/* <MyList store={store} /> */}
 
 const styles = StyleSheet.create({
   header: {
     height: Platform.OS === 'ios' ? 76 : 100,
     marginTop: Platform.OS === 'ios' ? 0 : 24,
     ...Platform.select({
       ios: {backgroundColor: 'red'},
       android: {backgroundColor: 'blue'},
     }),
     alignItems: 'center',
     justifyContent: 'center',
   },
   text: {
     color: 'white',
     fontSize: 24,
   },
 });
 
 export default App;
 