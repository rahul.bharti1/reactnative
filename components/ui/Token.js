/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-lone-blocks */
/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {
  Image,
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,ActivityIndicator,
  Platform,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import {icons} from '../utils/Images';

let url = {uri: 'https://reactjs.org/logo-og.png'};

class Token extends Component {
  constructor(props) {
    super(props);
   this.state = {
      isLoading: false,
      emp_name: '',
      mobile: '',
      designation: '',
      user_type: '',
      access_token:'',
      division_name:'',
    };
      // Get Read Data from DB
      this.readData().then((data) => {
        console.log('Read Data' + data);
    });
  }

  getToken = () => {
    // this.setState({isLoading: true});
    // this.props.geLoginAPI(email, pass);
  };

  readData = async () => {
    try {
     let emp_name = await AsyncStorage.getItem('emp_name');
     let emp_email = await AsyncStorage.getItem('emp_email');
     let mobile = await AsyncStorage.getItem('mobile');
     let designation = await AsyncStorage.getItem('designation');
     let division_name = await AsyncStorage.getItem('division_name');

     this.setState({emp_name: emp_name});
     this.setState({emp_email: emp_email});
     this.setState({mobile: mobile});
     this.setState({designation: designation});
     this.setState({division_name: division_name});

     this.setState({isDataLoaded: true});
    } catch (error) {
      alert(error);
    }
  }


  componentDidMount(){
   console.log('token = ' +  this.props.route.params.token);
  }
  componentDidUpdate() {
    // console.log('componentDidUpdate API');
    // this.state.isLoading ? this.navigateToScreen() : null;
  }
  render() {
    console.log('token render');
    return (
      <LinearGradient colors={['#FEFCFF', '#FEFCFF', '#FEFCFF']} style={styles.linearGradient}>
      <ScrollView style={{marginTop: '0%', flex: 1, flexDirection: 'column'}}>
        {/* ROOT */}
        <View style={[styles.container]}>

        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#D16223'}}>N </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#489DC7'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#E8C24C'}}>R </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'green'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'orange'}}>S</Text>
</View>
<View>
  <Text style={{textAlign: 'center', marginTop: 10,  fontWeight: 'bold'}}>T O K E N</Text>
</View>

{
  this.state.isLoading
          ?
          <ActivityIndicator size="small" color="#8000FBFE" />
          :
          <TouchableOpacity
            onPress={() => this.getToken()}
            style={[styles.button, styles.marginTop_20]}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
          {this.props.route.params.token}
            </Text>
          </TouchableOpacity>
}

<View style = {[styles.borderInner]}>

<View>
  <Text style={{textAlign: 'left'}}>{this.state.division_name}</Text>
</View>
<View>
  <Text style={{textAlign: 'left',  marginTop: 20, color: '#C0C0C0'}}>{this.state.emp_name}</Text>
</View>
<View>
  <Text style={{textAlign: 'left', color: '#C0C0C0'}}>{this.state.designation}</Text>
</View>
          {/* NAME */}
          <View style={[ styles.flexRowDirect, {marginTop: 15}] }>
            <Image style={[styles.viewHW]} source={icons.user} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
           {this.state.emp_name}
            </Text>
          </View>

          {/* number  */}
          <View
            style={[styles.flexRowDirect, styles.marginTop_20]}>
            <Image style={[styles.viewHW]} source={icons.call} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
          {this.state.mobile}
            </Text>
          </View>
           {/* Email  */}
           <View
            style={[styles.flexRowDirect, styles.marginTop_20]}>
            <Image style={[styles.viewHW]} source={icons.email} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
            {this.state.emp_email}
            </Text>
          </View>

        </View>
</View>

      </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  viewHeight: {
    height: 40,
  },
  viewHW: {
    height: 20,
    width: 20,
    alignSelf: 'center',
  },
  imageMargin: {
    ...Platform.select({ios: {marginStart: 0}, android: {marginStart: 10}}),
  },
  container: {
    margin: 5,
    padding: 10,
    borderRadius: 0,
    ...Platform.select({
      ios: {backgroundColor: 'white'},
      android: {backgroundColor: 'white'},
    }),
    // justifyContent:'center'
    // alignSelf: 'center'
  },

  button: {
    justifyContent: 'center',
    height: 40,
    borderRadius: 5,
    ...Platform.select({
      ios: {backgroundColor: 'green'},
      android: {backgroundColor: 'green'},
    }),
  },
  margin: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
  },
  marginHorizontal: {marginHorizontal: 10},
  marginStart_10: {marginStart: 10},
  marginTop_20: {marginTop: 20},
  marginEnd: {marginEnd: 10},
  marginBottom: {marginBottom: 10},

  flexRowDirect: {flexDirection: 'row'},

  paragraph: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paragraphCenter: {
    textAlign: 'center',
  },
  border: {
    borderWidth: 2,
    borderColor: '#7397F5',
    borderRadius: 6,
    ...Platform.select({
      ios: {padding: 10},
      android: {padding: 0},
    }),
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 1,
    paddingRight: 1,
    borderRadius: 0,
  },
  borderInner: {
    shadowColor: '#000',
    shadowOpacity: 0.30,
    shadowRadius: 6,
    elevation:10,
    borderWidth: 0,
    borderColor: '#7397F5',
    padding: 15,
    marginTop: 15,
    borderRadius: 6,
    ...Platform.select({
      ios: { backgroundColor: 'white'},
      android: { backgroundColor: 'white'},
    }),
  },
});


export default Token;
