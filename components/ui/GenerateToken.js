/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-lone-blocks */
/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import { connect } from 'react-redux';


import {
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,ActivityIndicator,
  Platform,
  BackHandler,
  Alert,
} from 'react-native';
import {notifyMessage} from '../utils/ExtensionFunc';
import LinearGradient from 'react-native-linear-gradient';
import { DrawerActions } from '@react-navigation/native';
import {loadTokenData} from '../api/action/TokenAction';
import AsyncStorage from '@react-native-community/async-storage';
import { Constant } from '../utils/Constant';
import {icons} from '../utils/Images';

let url = {uri: 'https://reactjs.org/logo-og.png'};

class GenerateToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded : false,
      isLoading: false,
      isStatus: true,
      type: 'APP',
      user_id: '',
      timestamp: '',
      emp_name: '',
mobile: '',
designation: '',
user_type: '',
access_token:'',
department:'',
division_name:'',
tstatus: '',
    };

    // Get Read Data from DB
    this.readData().then((data) => {
        console.log('Read Data' + data);
    });
  }

  getToken = (user_id, access_token) => {
    console.log('user Id : ' + user_id + ', access = ' + access_token + ', mType =' + this.state.type);
    this.setState({isLoading: true});
    this.props.geTokenAPI(user_id , access_token, this.state.type);
  };

  componentDidUpdate(prevProps) {
    // console.log('componentDidUpdate API');
     if (prevProps.tokenData !== this.props.tokenData ){
       this.navigateToScreen();
    }
  }

  navigateToScreen = () => {
    if ( this.props.tokenData.type === 'TRUE') {
      let message = this.props.tokenData.message;
      this.setState({isLoading: false});
      notifyMessage(message);
      this.props.navigation.navigate('Token', {token: this.props.tokenData.value});
    } else if (this.props.tokenData.message != null){
      this.setState({isLoading: false});
      notifyMessage(this.props.tokenData.message);
    } else {
      this.setState({isLoading: false});
      notifyMessage('Something went wrong');
    }
  }

   backAction = () => {
    Alert.alert("Hold on!", "Are you sure you want to go exit?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () => BackHandler.exitApp() }
    ]);
    return true;
  };

   backHandler = BackHandler.addEventListener(
    "hardwareBackPress",
    this.backAction
  );

  readData = async () => {
    try {
     let userId = await AsyncStorage.getItem('userId');
     let timestamp = await AsyncStorage.getItem('timestamp');
     let emp_name = await AsyncStorage.getItem('emp_name');
     let emp_email = await AsyncStorage.getItem('emp_email');
     let mobile = await AsyncStorage.getItem('mobile');
     let designation = await AsyncStorage.getItem('designation');
     let user_type = await AsyncStorage.getItem('user_type');
     let access_token = await AsyncStorage.getItem('access_token');
     let department = await AsyncStorage.getItem('department');
     let division_name = await AsyncStorage.getItem('division_name');
     let tstatus = await AsyncStorage.getItem('tstatus');

     console.log('Date ' + timestamp);

     this.setState({user_id: userId});
     this.setState({timestamp: timestamp});
     this.setState({emp_name: emp_name});
     this.setState({emp_email: emp_email});
     this.setState({mobile: mobile});
     this.setState({designation: designation});
     this.setState({user_type: user_type});
     this.setState({access_token: access_token});
     this.setState({department: department});
     this.setState({division_name: division_name});
     this.setState({tstatus: tstatus});

     this.setState({isDataLoaded: true});
    } catch (error) {
      alert(error);
    }
  }

   logout = () => {
  this.props.navigation.navigate('Splash');
}

  render() {
    console.log('token render');
    return (
      <LinearGradient colors={['#FEFCFF', '#FEFCFF', '#FEFCFF']} style={styles.linearGradient}>
      <ScrollView style={{marginTop: '0%', flex: 1, flexDirection: 'column'}}>
        {/* Toolbar */}
        <View style={[styles.container]}>
        <View style={{flexDirection: 'row', backgroundColor: 'white' , elevation: 0,  shadowOpacity: 0, shadowRadius: 6,padding: 10}}>
           <TouchableOpacity style={{flex: 0.5, alignSelf: 'center'}}
            onPress={() =>  this.props.navigation.dispatch(DrawerActions.openDrawer())} >
            <Image style={{width: 20, margin: 5, height: 20, alignSelf: 'flex-start'} } source={icons.menu} />
           </TouchableOpacity>

<View  style={{flex: 2, alignSelf: 'center'}}>
            <Text style={{color: 'black', fontWeight: 'bold', fontStyle: 'italic',marginHorizontal: 10, alignSelf: 'center'}}> </Text>
</View>
</View>


<View style={{flexDirection: 'row', alignSelf: 'center'}}>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#D16223'}}>N </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#489DC7'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: '#E8C24C'}}>R </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'green'}}>W </Text>
  <Text style={{textAlign: 'center', fontSize: 18,  fontWeight: 'bold', color: 'orange'}}>S</Text>
</View>

<View style = {[styles.borderInner, {marginStart: 15, marginEnd: 15}]}>

<View>
  <Text style={{textAlign: 'left',color: 'black'}}>{this.state.division_name}</Text>
</View>
<View>
  <Text style={{textAlign: 'left', color: '#C0C0C0', marginTop: 20}}>
  {
             this.state.isDataLoaded
             ?
            this.state.emp_name
             :
             'Alka'
             }
  </Text>
</View>
<View>
  <Text style={{textAlign: 'left', color: '#C0C0C0'}}>
  {
             this.state.isDataLoaded
             ?
            this.state.designation
             :
             'Assistance Engineer'
             }
  </Text>
</View>
          {/* NAME */}
          <View style={[ styles.flexRowDirect, {marginTop: 15}] }>
            <Image style={[styles.viewHW]} source={icons.user} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
            {
             this.state.isDataLoaded
             ?
            this.state.emp_name
             :
             'Alka'
             }
            </Text>
          </View>

          {/* number  */}
          <View
            style={[styles.flexRowDirect, styles.marginTop_20]}>
            <Image style={[styles.viewHW]} source={icons.call} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
            {
             this.state.isDataLoaded
             ?
            this.state.mobile
             :
             '8171677544'
             }
            </Text>
          </View>
           {/* Email  */}
           <View
            style={[styles.flexRowDirect, styles.marginTop_20]}>
            <Image style={[styles.viewHW]} source={icons.email} />
            <Text  style={[styles.marginHorizontal, {color: '#C0C0C0'}]}>
            {
             this.state.isDataLoaded
             ?
            this.state.emp_email
             :
             'alka@gmail.com'
             }
            </Text>
          </View>

        </View>

{
  this.state.isLoading
          ?
          <ActivityIndicator size="small" color="#8000FBFE"  style={{margin: 10}}/>
          :
          <TouchableOpacity
            onPress={() => this.getToken(this.state.user_id, this.state.access_token)}
            style={[styles.button, {flexDirection: 'row', marginTop: 20}]}>
            <Image style={[styles.viewHW, styles.imageMargin]} source={icons.key} />
            <Text
              style={{
               alignSelf: 'center', marginLeft: 10,
                color: 'white',
                fontSize: 14,
                fontWeight: 'normal',
              }}>
              GET TOKEN
            </Text>
          </TouchableOpacity>
}
{/*  */}

{/* <TouchableOpacity
            // onPress={() => this.getToken()}
            style={[styles.button, {flexDirection: 'row', marginTop: 20, backgroundColor: 'orange'}]}>
            <Text
              style={{
                textAlign: 'center', textAlignVertical: 'center', marginLeft: 10,
                color: 'white',
                fontSize: 14,
                fontWeight: 'normal',
              }}>
              TOKEN SYSTEM VALIDATION
            </Text>
          </TouchableOpacity> */}

{/* For days and date */}
{/* <View style={{flexDirection: 'row', padding: 15}}> */}
{/* For days */}
{/* <View style = {[styles.borderInner, {flex: 1}]}>
    <View>
    <Image style={[styles.viewHW, styles.imageMargin]} source={url} />
            <Text
              style={{
                textAlign: 'center', textAlignVertical: 'center', marginTop: 5,
                color: 'black',
                fontSize: 14,
                fontWeight: 'normal',
              }}>
            6 Days
            </Text>
    </View>
</View> */}

{/* For date */}
{/* <View style = {[styles.borderInner, {flex: 1.5, marginStart: 10}]}>
    <Image style={[styles.viewHW, styles.imageMargin]} source={url} />
            <Text
              style={{
                textAlign: 'center', textAlignVertical: 'center', marginTop: 5,
                color: 'black',
                fontSize: 14,
                fontWeight: 'normal',
              }}>

             {
             this.state.isDataLoaded
             ?
            this.state.timestamp
             :
             '07/07/2021'
             }

            </Text>
</View> */}
{/* </View> */}

{/* status */}
<View style={{flexDirection: 'row', marginTop: 20}}>
  <Text style={{textAlign: 'left', marginEnd: 10,  textAlignVertical: 'center',marginStart: 20, color: 'black'}}>Status</Text>
   <Text style={[{backgroundColor: this.state.tstatus === 'TRUE' ?  'green' :  '#C0C0C0', paddingStart: 5, paddingEnd: 5, paddingTop: 2, paddingBottom: 2, borderRadius: 15,textAlign: 'left', color: 'white'}]}>Active</Text>
</View>

</View>
      </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  viewHeight: {
    height: 40,
  },
  viewHW: {
    height: 20,
    width: 20,
    alignSelf: 'center',
  },
  imageMargin: {
    ...Platform.select({ios: {marginStart: 0}, android: {marginStart: 0}}),
  },
  container: {
    margin: 0,
    padding: 0,
    borderRadius: 0,
    ...Platform.select({
      ios: {backgroundColor: 'white'},
      android: {backgroundColor: 'white'},
    }),
    // justifyContent:'center'
    // alignSelf: 'center'
  },

  button: {
    justifyContent: 'center',
    height: 40,
    marginLeft: 15,
    marginEnd: 15,
    borderRadius: 5,
    ...Platform.select({
      ios: {backgroundColor: 'green'},
      android: {backgroundColor: 'green'},
    }),
  },
  margin: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
  },
  marginHorizontal: {marginHorizontal: 10},
  marginStart_10: {marginStart: 10},
  marginTop_20: {marginTop: 20},
  marginEnd: {marginEnd: 10},
  marginBottom: {marginBottom: 10},

  flexRowDirect: {flexDirection: 'row'},

  paragraph: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paragraphCenter: {
    textAlign: 'center',
  },
  border: {
    borderWidth: 2,
    borderColor: '#7397F5',
    borderRadius: 6,
    ...Platform.select({
      ios: {padding: 10},
      android: {padding: 0},
    }),
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 1,
    paddingRight: 1,
    borderRadius: 0,
  },
  borderInner: {
    shadowColor: '#000',
    shadowOpacity: 0.30,
    shadowRadius: 6,
    elevation:5,
    borderWidth: 0,
    borderColor: '#7397F5',
    padding: 15,
    marginTop: 20,
    borderRadius: 6,
    ...Platform.select({
      ios: { backgroundColor: 'white'},
      android: { backgroundColor: 'white'},
    }),
  },
});

const mapStateToProps = state => ({
  tokenData: state.tokenResponse,
});

const dispatchStateToProps = dispatch => ({
    geTokenAPI: (user_id, access_token, mType) => dispatch(loadTokenData(user_id, access_token, mType)),
});

export default connect(mapStateToProps, dispatchStateToProps)(GenerateToken);
