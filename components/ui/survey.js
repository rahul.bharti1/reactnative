/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/react-in-jsx-scope */
import React, {Component} from 'react';
import {
  StyleSheet,
  Platform,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  Image,
  View,
  FlatList,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
let url = {uri: 'https://reactjs.org/logo-og.png'};
import {MyToolbar} from '../utils/ExtensionFunc';

export default class Clearances extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {/* <MyToolbar title={'Survey'} /> */}
        {/* <MyToolbar title={'Survey'} /> */}
        <FlatList
          data={['2', '3']}
          numColumns={1}
          renderItem={({item, index}) => (
            <View style={{flex: 1}}>
              <LinearGradient
                start={{x: 0, y: 0}}
                locations={[0.5, 0.6]}
                colors={['orange', 'yellow']}
                style={[styles.border]}>
                <TouchableOpacity
                  style={{flex: 1, flexDirection: 'row'}}
                  onPress={() => alert('dcdc')}>
                  <Image
                    style={{
                      borderRadius: 50 / 2,
                      overflow: 'hidden',
                      width: 50,
                      height: 50,
                      alignSelf: 'flex-start',
                      backgroundColor: 'black',
                    }}
                    source={url}
                  />
                </TouchableOpacity>
              </LinearGradient>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  viewHeight: {
    height: 40,
  },
  text: {
    padding: 10,
    width: '100%',
    fontWeight: 'bold',
    fontStyle: 'italic',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    ...Platform.select({
      ios: {backgroundColor: 'white', color: 'red'},
      android: {backgroundColor: 'white', color: 'blue'},
      default: {backgroundColor: 'blue'},
    }),
  },
  viewHW: {
    height: 25,
    width: 25,
  },
  container: {
    margin: 15,
    padding: 24,
    borderRadius: 10,
    ...Platform.select({
      ios: {backgroundColor: 'white'},
      android: {backgroundColor: 'white'},
    }),
    // justifyContent:'center'
    // alignSelf: 'center'
  },
  button: {
    borderRadius: 10,
    padding: 10,
    ...Platform.select({
      ios: {backgroundColor: 'red'},
      android: {backgroundColor: 'blue'},
    }),
  },
  margin: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
  },
  marginHorizontal: {marginHorizontal: 10},
  marginStart_10: {marginStart: 10},
  marginTop_20: {marginTop: 20},
  marginTop_5: {marginTop: 5},
  marginEnd: {marginEnd: 10},
  marginBottom: {marginBottom: 10},
  flexRowDirect: {flexDirection: 'row'},
  paragraph: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
    fontSize: 16,
    textAlign: 'center',
  },
  paragraphCenter: {
    textAlign: 'center',
  },
  border: {
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 10,
    borderWidth: 0,
    borderColor: '#7397F5',
    borderRadius: 6,
    marginVertical: 5,
    marginHorizontal: 8,
    ...Platform.select({
      ios: {padding: 10, backgroundColor: 'white'},
      android: {padding: 10, backgroundColor: 'white'},
    }),
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});
