/* eslint-disable prettier/prettier */
import {StyleSheet, Platform} from '.react-native';

// eslint-disable-next-line no-unused-vars
export const styles = StyleSheet.create({
    header: {
        height: Platform.OS === 'ios' ? 76 : 100,
        marginTop: Platform.OS === 'ios' ? 0 : 24,
        ...Platform.select({
          ios: {backgroundColor: 'red'},
          android: {backgroundColor: 'blue'},
        }),
        alignItems: 'center',
        justifyContent: 'center',
      },
      text: {
        color: 'white',
        fontSize: 24,
      },
});
