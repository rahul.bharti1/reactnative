/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-lone-blocks */
/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  BackHandler,
  Alert,
  TouchableOpacity,ActivityIndicator,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import LinearGradient from 'react-native-linear-gradient';
import {notifyMessage} from '../../utils/ExtensionFunc';
import {loadLoginData} from '../../api/action/loginaction';
import { connect } from 'react-redux';
let url = {uri: 'https://reactjs.org/logo-og.png'};
import {icons} from '../../utils/Images';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLogin: false,
    };
  }

  login = (email, pass) => {
    if (email === '') {
      notifyMessage('Please enter email id');
      return;
    }

    if (pass === '') {
      notifyMessage('Please enter password');
      return;
    }
    this.setState({isLogin: true});
    this.props.geLoginAPI(email , pass);
  };

  componentDidUpdate(prevProps) {
    // eslint-disable-next-line keyword-spacing
    if(prevProps.loginData !== this.props.loginData ){
       this.navigateToScreen();
    }
    }

    saveData = async (data) => {
      try {

        await  AsyncStorage.setItem('userId',data.user_id);
        await  AsyncStorage.setItem('emp_name',data.emp_name);
        await  AsyncStorage.setItem('emp_email',data.emp_email);
        await  AsyncStorage.setItem('mobile',data.mobile);
        await  AsyncStorage.setItem('designation',data.designation);
        await  AsyncStorage.setItem('timestamp',data.timestamp);
        await  AsyncStorage.setItem('user_type',data.user_type);
        await  AsyncStorage.setItem('profile_pic',data.profile_pic);
        await  AsyncStorage.setItem('access_token',data.access_token);
        await  AsyncStorage.setItem('department',data.department);
        await  AsyncStorage.setItem('division_name',data.division_name);
        await  AsyncStorage.setItem('tstatus',data.tstatus);
        await  AsyncStorage.setItem('isLogin','true');

        this.props.navigation.navigate('HomeController');

      } catch (error) {
        alert(error);
      }
    }

  navigateToScreen = () => {
    if ( this.props.loginData.type === 'TRUE') {
      let message = this.props.loginData.message;
      this.setState({isLogin: false});
      notifyMessage(message);
      // let obj = {
      //   name: 'Michal',
      //   email: 'michal@gmail.com',
      //   city: 'New York',
      // };
      // AsyncStorage.setItem('user',JSON.stringify(obj));
    this.saveData(this.props.loginData.value);

    } else if (this.props.loginData.message != null){
      this.setState({isLogin: false});
      notifyMessage(this.props.loginData.message);
    } else {
      this.setState({isLogin: false});
      notifyMessage('Something went wrong');
    }
  }

  backAction = () => {
    Alert.alert('Hold on!', 'Are you sure you want to go exit?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      { text: 'YES', onPress: () => BackHandler.exitApp() },
    ]);
    return true;
  };

   backHandler = BackHandler.addEventListener(
    'hardwareBackPress',
    this.backAction
  );

  render() {
    return (
       <LinearGradient colors={['white', '#00BCD4', '#00BCD4']} style={styles.linearGradient}>
      {/* <ScrollView> */}
        <View style={{height: '100%',justifyContent: 'center'}}>

        {/* ROOT */}
        <View style={[styles.container]}>
          {/* <Text style={styles.paragraphCenter}> {this.props.username} </Text> */}
          {/* <View style={[styles.separator]} /> */}

          {/* email id */}
          <View style={[styles.border, styles.flexRowDirect]}>
            <Image style={[styles.viewHW, styles.imageMargin]} source={icons.user} />
            <TextInput
              style={[styles.marginHorizontal, { width:'80%'}]}
              placeholder="Email id"
              autoCapitalize="none"
              placeholderTextColor="#999"
              onChangeText={getEmail => {
                this.setState({email: getEmail});
              }}
           />
            {/* gaurav.chaudhary@kreatetechnologies.com */}
          </View>

          {/* password  */}
          <View
            style={[styles.border, styles.flexRowDirect, styles.marginTop_20]}>
            <Image style={[styles.viewHW, styles.imageMargin]} source={icons.email} />
            <TextInput
              onChangeText={getPass => {
                this.setState({password: getPass});
              }}
              style={[styles.marginHorizontal,  { width:'80%'}]}
              placeholder="Password"
            />
            {/* 1234 */}
          </View>
             {/* Login Button  */}
{
  this.state.isLogin ?
          <ActivityIndicator size="small" color="#0000ff" style={{marginTop: 20}}/>
          :
          <TouchableOpacity
            onPress={() => this.login(this.state.email, this.state.password)}
            style={[styles.button, styles.marginTop_20]}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Login
            </Text>
          </TouchableOpacity>
}
        </View>
        </View>
       {/* </ScrollView> */}
       </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  viewHeight: {
    height: 40,
  },
  viewHW: {
    height: 20,
    width: 20,
    alignSelf: 'center',
  },
  imageMargin: {
    ...Platform.select({ios: {marginStart: 10}, android: {marginStart: 10}}),
  },
  container: {
    margin: 15,
    alignSelf: 'center',
    padding: 24,
    borderRadius: 10,
    ...Platform.select({
      ios: {backgroundColor: 'white'},
      android: {backgroundColor: 'white'},
    }),
    // justifyContent:'center'
    // alignSelf: 'center'
  },

  button: {
    justifyContent: 'center',
    height: 40,
    borderRadius: 5,
    ...Platform.select({
      ios: {backgroundColor: 'green'},
      android: {backgroundColor: 'green'},
    }),
  },
  margin: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
  },
  marginHorizontal: {marginHorizontal: 10},
  marginStart_10: {marginStart: 10},
  marginTop_20: {marginTop: 20},
  marginEnd: {marginEnd: 10},
  marginBottom: {marginBottom: 10},

  flexRowDirect: {flexDirection: 'row'},

  paragraph: {
    marginTop: 10,
    marginEnd: 20,
    marginStart: 20,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paragraphCenter: {
    textAlign: 'center',
  },
  border: {
    borderWidth: 2,
    borderColor: 'green',
    borderRadius: 6,
    ...Platform.select({
      ios: {padding: 10},
      android: {padding: 0},
    }),
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 0,
  },
});

const mapStateToProps = state => ({
  loginData: state.loginResponse,
});

const dispatchStateToProps = dispatch => ({
    geLoginAPI: (username, password) => dispatch(loadLoginData(username, password)),
});

export default connect(mapStateToProps, dispatchStateToProps)(Login);
