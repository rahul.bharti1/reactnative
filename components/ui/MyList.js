/* eslint-disable no-sequences */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import React , { Component } from 'react';
import {
    StyleSheet, Alert,Platform,ActivityIndicator,
    Text, TouchableOpacity,Image,TextInput,
    View,
    FlatList,
} from 'react-native';
import { loadCountryData } from '../api/action/action';
import { connect } from 'react-redux';
import { DrawerActions } from '@react-navigation/native';


let url = {uri: 'https://reactjs.org/logo-og.png'};

 class MyList extends Component {
constructor(props) {
    super(props);
    this.state = {
      search: '',
      data: [],
      input: '',
      isLoader: true,
      modelVisiblity: false,
      propsData: 'I am Modal',
    };
  }
  componentDidMount = () => {
    console.log('componentDidMount MyList');
    this.state.isLoader = false,
    this.props.getCountriesAPI();
  }
//   componentDidUpdate(prevProps) {
//    // eslint-disable-next-line eqeqeq
//     if (this.props.countryData != prevProps.countryData) {
//         if (this.props.countryData.type == 'TRUE') {
//             this.setState({
//                 isLoader: false,
//             });
//         }
//     }
// }
    render() {
        console.log('mylist_render');
        return (
            <View  style={{ height: '100%' }}>
           {/* Header */}
          <View style={{flexDirection: 'row', backgroundColor: 'white' , elevation: 10,  shadowOpacity: 0.30, shadowRadius: 6,padding: 10}}>
           <TouchableOpacity style={{flex: 0.5, alignSelf: 'center'}}
            onPress={() =>  this.props.navigation.dispatch(DrawerActions.openDrawer())} >
            <Image style={{width: 25, height: 25, alignSelf: 'flex-start'} } source={url} />
           </TouchableOpacity>

<View  style={{flex: 2, alignSelf: 'center'}}>
            <Text style={{color: 'black', fontWeight: 'bold', fontStyle: 'italic',marginHorizontal: 10, alignSelf: 'center'}}>Hello World</Text>
</View>

<View  style={{flex: 0.8, alignSelf: 'center'}}>
            <TouchableOpacity
            // eslint-disable-next-line no-alert
            onPress={() =>   this.props.navigation.goBack() }
            style={styles.button}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 16,
              }}>
              Logout
            </Text>
          </TouchableOpacity>
</View>

          </View>
               {/* Heading */}
          <Text style={[styles.text, styles.marginTop_5]}>Countries</Text>

{this.state.isLoader && this.props.countryData.value == null ?   <ActivityIndicator size="large" color="#00ff00" /> : null}

                <FlatList
                    horizontal={true}
                    data={this.props.countryData.value}
                    renderItem={({ item, index }) => (
                      <View style={{width: 50, height: 50, marginBottom: 30, marginTop: 10, marginLeft: 10, borderColor: 'green', borderWidth:1, borderRadius: 50 / 2 }} >
                         <Image style={{overflow: 'hidden', borderRadius: 50 / 2, width: 50, height: 50}} source={{uri : item.flag_img}} />
                      </View>
                    )}
                  />
              {/* my list */}
                <FlatList
                    data={this.props.countryData.value}
                    style={{ width: '100%', alignSelf: 'center', marginLeft: 5, marginRight: 5 }}
                    renderItem={({ item, index }) => (
                      <TouchableOpacity onPress={() => Alert.alert(item.name)}>
                        <View style={[styles.border, styles.flexRowDirect]}>
{/* first view */}
                         <View  style={{flex: 2,justifyContent: 'flex-start', backgroundColor: 'white', flexDirection: 'column'}}>
                         <Text>Name : {item.name}</Text>
                         <Text>Num Code : {item.numcode}</Text>
                         <Text>Latitude : {item.latitude}</Text>
                         <Text>Longitude : {item.longitude}</Text>
                         </View>
{/* second view */}
                         <View style={{flex: 1, backgroundColor: 'white', justifyContent: 'flex-start'}} >
                         <Image  style={{width: 50, height: 50, alignSelf: 'flex-end'}} source={{uri : item.flag_img}} />
                         </View>
                      </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        );
    }
 }

 const styles = StyleSheet.create({
    viewHeight: {
      height: 40,
    },
    text: {
      padding: 10,width: '100%',
      fontWeight: 'bold', fontStyle: 'italic',fontSize: 20,textAlign: 'center', alignSelf: 'center',
      ...Platform.select({
        ios: {backgroundColor: 'white' , color: 'red'},
        android: {backgroundColor: 'white', color: 'blue'},
        default: {  backgroundColor: 'blue' },
      }),
    },
    viewHW: {
      height: 25,
      width: 25,
    },
    container: {
      margin: 15,
      padding: 24,
      borderRadius: 10,
      ...Platform.select({
        ios: {backgroundColor: 'white'},
        android: {backgroundColor: 'white'},
      }),
      // justifyContent:'center'
      // alignSelf: 'center'
    },
    button: {
      borderRadius: 10,
      padding: 10,
      ...Platform.select({
        ios: {backgroundColor: 'red'},
        android: {backgroundColor: 'blue'},
      }),
    },
    margin: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
    },
    marginHorizontal: {marginHorizontal: 10},
    marginStart_10: {marginStart: 10},
    marginTop_20: {marginTop: 20},
    marginTop_5: {marginTop: 5},
    marginEnd: {marginEnd: 10},
    marginBottom: {marginBottom: 10},
    flexRowDirect: {flexDirection: 'row'},
    paragraph: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
      fontSize: 16,
      textAlign: 'center',
    },
    paragraphCenter: {
      textAlign: 'center',
    },
    border: {
      shadowColor: '#000',
      shadowOpacity: 0.30,
      shadowRadius: 6,
      elevation:10,
      borderWidth: 0,
      borderColor: '#7397F5',
      borderRadius: 6,
      ...Platform.select({
        ios: {padding: 10, margin: 10, backgroundColor: 'white'},
        android: {padding: 10, margin: 10, backgroundColor: 'white'},
      }),
    },
    separator: {
      marginVertical: 8,
      borderBottomColor: '#737373',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
  });

const mapStateToProps = state => ({
    countryData: state.countryDataResponse,
});

const dispatchStateToProps = dispatch => ({
    getCountriesAPI: () => dispatch(loadCountryData()),
});

export default connect(mapStateToProps, dispatchStateToProps)(MyList);

// export default MyList;
