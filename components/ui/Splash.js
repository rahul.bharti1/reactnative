/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-lone-blocks */
/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {
  Image,
  Text,
  View,
  StyleSheet,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class Splash extends Component {
   constructor(){
     super();
     this.state = {
     isVisible : true,
     isLogin : false,
    };
  }

  readData = async () => {
    try {
     let Login = await AsyncStorage.getItem('isLogin');

     setTimeout(() => {
        if (Login === 'true'){
            this.props.navigation.navigate('HomeController');
        } else {
            this.props.navigation.navigate('Login');
        }
    }, 2000);
    } catch (error) {
      alert(error);
      this.setState({isLogin: false});
    }
  }

  componentDidMount(){
    // Get Read Data from DB
    this.readData().then((data) => {
        console.log('Read Data' + data);
    });
   }

    render()
    {
        let Splash_Screen = (
             <View style={styles.SplashScreen_RootView}>
                 <View style={styles.SplashScreen_ChildView}>
                    <Text style={{fontSize: 18}}>Narmada</Text>
                </View>
             </View> );
         return (
             <View style = { styles.MainContainer }>
                <Text style={{textAlign: 'center'}}> Splash Screen Example</Text>
                 {
                    Splash_Screen
                }
            </View>
              );
    }
}
 const styles = StyleSheet.create(
{
    MainContainer:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0,
    },

    SplashScreen_RootView:
    {
        justifyContent: 'center',
        flex:1,
        margin: 10,
        position: 'absolute',
        width: '100%',
        height: '100%',
      },

    SplashScreen_ChildView:
    {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00BCD4',
        flex:1,
    },
});
