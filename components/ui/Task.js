/* eslint-disable no-fallthrough */
/* eslint-disable no-alert */
/* eslint-disable no-sequences */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import React , { Component } from 'react';
import {
    StyleSheet, Alert,Platform,ActivityIndicator,
    Text, TouchableOpacity,Image,TextInput,
    View,Switch,
    FlatList,
    ImageStore,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { loadCountryData} from '../api/action/action';
import {loadTaskData} from '../api/action/taskaction';
import { connect } from 'react-redux';
import { Icon } from 'react-native-vector-icons/Icon';
import {SwitchExample} from '../utils/ExtensionFunc';


let url = {uri: 'https://reactjs.org/logo-og.png'};

 class Task extends Component {
constructor(props) {
    super(props);
    this.state = {
      isLoader: true,
      switch1Value: false,
      listColor: 'black',
    };
  }
  componentDidMount = () => {
    console.log('componentDidMount  TASK');
    this.state.isLoader = false,
    this.props.geTaskAPI('NHPC');
  }
  componentDidUpdate(prevProps) {
   // eslint-disable-next-line eqeqeq
    if (this.props.taskData != prevProps.taskData) {
        if (this.props.taskData.type == 'TRUE') {
          console.log('componentDidUpdate  TRUE  TASK');
            this.setState({
                isLoader: false,
            });
        } else if (this.props.taskData.type == 'FALSE'){
          console.log('componentDidUpdate  FALSE  TASK');
        }
    }
}
toggleSwitch1 = (value) => {
  this.setState({switch1Value: value});
  console.log('Switch 1 is: ' + value);
}

getColor = (value) => {
switch (value){
 case 'Pre Construction':
  return 'white';
 case 'Construction':
  return 'green';
 case 'Survey & Investigation':
  return 'blue';}
}

clickEvent = (item, index) => {
  switch (item.PROJECT_TYPE){
   case 'Pre Construction':
    alert(item.PROJECT_TYPE + ', Index = ' + index);
    break;
   case 'Construction':
    alert(item.PROJECT_TYPE + ' Index = ' + index);
    this.props.navigation.navigate('Clearances');
    break;
   case 'Survey & Investigation':
    alert(item.PROJECT_TYPE + ' Index = ' + index);
    break;
  }
}

    render() {
        console.log('Task');
        return (
          <View  style={{ height: '100%' }}>
          <MyToolbar title={'Task'} />
               {/* Heading */}
          <Text style={[{color: 'blue',marginHorizontal: 10 , marginVertical: 20, fontSize: 20}]}>Select Item</Text>

          {/* <Switch
        trackColor={{ false: '#767577', true: '#81b0ff' }}
        thumbColor={this.state.switch1Value ? '#f5dd4b' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={this.toggleSwitch1}
        value={this.state.switch1Value}
      /> */}
       {/* <SwitchExample
             toggleSwitch1 = {this.toggleSwitch1}
             switch1Value = {this.state.switch1Value}/>  */}
{this.state.isLoader && this.props.taskData.data == null ?   <ActivityIndicator size="large" color="#00ff00" /> : null}

              {/* my list */}
                <FlatList
                    data={this.props.taskData.data}
                    numColumns={2}
                    renderItem={({ item, index }) => (
                      <View style={{flex: 1}}>
                      <LinearGradient
                      start={{ x: 0, y: 0 }}
                      locations={[0.5, 0.6]}
                      colors={['orange', this.getColor(item.PROJECT_TYPE)]}
                      style={[styles.border]}>

                      <TouchableOpacity style={{flex: 1,  flexDirection: 'row'}} onPress={() => this.clickEvent(item, index)}>
                        <Image style={{borderRadius: 50 / 2, overflow: 'hidden',width: 50,height: 50, alignSelf: 'flex-start', backgroundColor: 'black'}}  source={url} />
                        <View style={{marginStart: 10, alignSelf: 'center'}}>
                        <Text style={{fontSize: 10, color: 'orange'}}>{item.PROJ_NAME}</Text>
                          <Text style={{fontSize: 10,  color: 'orange'}}>{item.PROJECT_TYPE}</Text>
                        </View>
                        </TouchableOpacity>
                        </LinearGradient>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
 }

 const styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
    },
    viewHeight: {
      height: 40,
    },
    text: {
      padding: 10,width: '100%',
      fontWeight: 'bold', fontStyle: 'italic',fontSize: 20,textAlign: 'center', alignSelf: 'center',
      ...Platform.select({
        ios: {backgroundColor: 'white' , color: 'red'},
        android: {backgroundColor: 'white', color: 'blue'},
        default: {  backgroundColor: 'blue' },
      }),
    },
    viewHW: {
      height: 25,
      width: 25,
    },
    container: {
      margin: 15,
      padding: 24,
      borderRadius: 10,
      ...Platform.select({
        ios: {backgroundColor: 'white'},
        android: {backgroundColor: 'white'},
      }),
      // justifyContent:'center'
      // alignSelf: 'center'
    },
    button: {
      borderRadius: 10,
      padding: 10,
      ...Platform.select({
        ios: {backgroundColor: 'red'},
        android: {backgroundColor: 'blue'},
      }),
    },
    margin: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
    },
    marginHorizontal: {marginHorizontal: 10},
    marginStart_10: {marginStart: 10},
    marginTop_20: {marginTop: 20},
    marginTop_5: {marginTop: 5},
    marginEnd: {marginEnd: 10},
    marginBottom: {marginBottom: 10},
    flexRowDirect: {flexDirection: 'row'},
    paragraph: {
      marginTop: 10,
      marginEnd: 20,
      marginStart: 20,
      fontSize: 16,
      textAlign: 'center',
    },
    paragraphCenter: {
      textAlign: 'center',
    },
    border: {
      shadowColor: '#000',
      shadowOpacity: 0.30,
      shadowRadius: 6,
      elevation:10,
      borderWidth: 0,
      borderColor: '#7397F5',
      borderRadius: 6,
      marginVertical: 5,
      marginHorizontal: 8,
      ...Platform.select({
        ios: {padding: 10, backgroundColor: 'white'},
        android: {padding: 10, backgroundColor: 'white'},
      }),
    },
    separator: {
      marginVertical: 8,
      borderBottomColor: '#737373',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
  });

const mapStateToProps = state => ({
  taskData: state.taskResponse,
});

const dispatchStateToProps = dispatch => ({
    geTaskAPI: (org) => dispatch(loadTaskData(org)),
});

export default connect(mapStateToProps, dispatchStateToProps)(Task);


export const MyToolbar = (props) => {
  return (
      <View style={{flexDirection: 'row', height: 60, backgroundColor: 'white' , elevation: 10,  shadowOpacity: 0.30, shadowRadius: 6,padding: 10}}>
       <TouchableOpacity style={{flex: 0.5, alignSelf: 'center'}}
               onPress={() => props.navigation.goBack() } >
            <Image style={{width: 25, height: 25, alignSelf: 'flex-start'} } source={url} />
           </TouchableOpacity>

        <View  style={{flex: 2, alignSelf: 'center'}}>
            <Text style={{color: 'black', fontWeight: 'bold', fontStyle: 'italic',marginHorizontal: 10}}>{props.title}</Text>
        </View>
       </View>
  );
};
