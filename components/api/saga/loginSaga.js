/* eslint-disable prettier/prettier */

import { put, call, takeEvery } from 'redux-saga/effects';
import { Constant } from '../../utils/Constant';
import { getLogin } from '../APICalling';
import { getLoginError, getLoginResponse } from '../../api/action/loginaction';

//worker saga
function* handleLoginDataResponse(action) {
    try {
        const data = yield call(getLogin, action.username, action.password);
        console.log(' Login Response', data);
        yield put(getLoginResponse(data));  // STEP 2 id success
    } catch (error) {
        yield put(getLoginError(error.toString())); // STEP 2 if failure
    }
}

//watcher saga
 function* loginDataWatcherSaga() {   // STEP 3 return fetch data
    yield takeEvery(Constant.LOGIN_DATA, handleLoginDataResponse);
}

export {loginDataWatcherSaga};
