/* eslint-disable prettier/prettier */

import { put, call, takeEvery } from 'redux-saga/effects';
import { Constant } from '../../utils/Constant';
import {getTask} from '../APICalling';
import { getTaskError, getTaskResponse } from '../../api/action/taskaction';


function* handleTaskDataResponse(action) {
    try {
        const data = yield call(getTask, action.org);
        console.log('Task Response', data);
        yield put(getTaskResponse(data));  // STEP 2 id success
    } catch (error) {
        yield put(getTaskError(error.toString())); // STEP 2 if failure
    }
}

//watcher saga
 function* TaskDataWatcherSaga() {   // STEP 3 return fetch data
    yield takeEvery(Constant.TASK_DATA, handleTaskDataResponse);
}

export {TaskDataWatcherSaga};
