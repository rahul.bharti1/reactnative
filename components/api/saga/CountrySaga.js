/* eslint-disable prettier/prettier */

import { put, call, takeEvery } from 'redux-saga/effects';
import { Constant } from '../../utils/Constant';
import { getCountries } from '../APICalling';
import { getError, getResponse } from '../../api/action/action';

//worker saga
function* handleCountryDataResponse(action) {
    try {
        const data = yield call(getCountries);
        console.log('Country Name Response', data);
        yield put(getResponse(data));  // STEP 2 id success
    } catch (error) {
        yield put(getError(error.toString())); // STEP 2 if failure
    }
}

//watcher saga
 function* CountryDataWatcherSaga() {   // STEP 3 return fetch data
    yield takeEvery(Constant.COUNTRY_DATA, handleCountryDataResponse);
}

export {CountryDataWatcherSaga};


