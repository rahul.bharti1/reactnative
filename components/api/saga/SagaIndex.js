import {all} from 'redux-saga/effects';
import {TaskDataWatcherSaga} from './TaskSaga';
import {loginDataWatcherSaga} from './loginSaga';
import {CountryDataWatcherSaga} from './CountrySaga';
import {tokenDataWatcherSaga} from './TokenSaga';

export default function* rootSaga() {
  yield all([
    CountryDataWatcherSaga(),
    TaskDataWatcherSaga(),
    loginDataWatcherSaga(),
    tokenDataWatcherSaga(),
  ]); // STEP 4  CountryDataWatcherSaga(),
}
