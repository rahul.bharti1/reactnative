/* eslint-disable prettier/prettier */

import { put, call, takeEvery } from 'redux-saga/effects';
import { Constant } from '../../utils/Constant';
import { getToken } from '../APICalling';
import { getTokenError, getTokenResponse } from '../../api/action/TokenAction';

//worker saga
function* handleTokenDataResponse(action) {
    try {
        console.log(' Token Response', data);
        const data = yield call(getToken, action.user_id, action.access_token, action.mType);
        yield put(getTokenResponse(data));  // STEP 2 id success
    } catch (error) {
        yield put(getTokenError(error.toString())); // STEP 2 if failure
    }
}

//watcher saga
 function* tokenDataWatcherSaga() {   // STEP 3 return fetch data
    yield takeEvery(Constant.TOKEN_DATA, handleTokenDataResponse);
}

export {tokenDataWatcherSaga};
