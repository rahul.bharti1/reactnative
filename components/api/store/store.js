import {createStore, applyMiddleware, compose} from 'redux';
// import rootReducers from '../reducer/CountryReducer';
import rootReducers from '../reducer/Reducer';

import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga/SagaIndex';
import {createLogger} from 'redux-logger';

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducers,
    compose(
      applyMiddleware(sagaMiddleware, createLogger()),
      //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ),
  );
  sagaMiddleware.run(rootSaga); // STEP 5
  // store.dispatch({ type: 'LOGIN' })
  return store;
};

export default configureStore;
