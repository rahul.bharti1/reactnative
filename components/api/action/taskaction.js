import {Constant} from '../../utils/Constant';
//  TASK API
const loadTaskData = org => ({
  type: Constant.TASK_DATA,
  org,
});

const getTaskResponse = payload => ({
  type: Constant.TASK_DATA_SUCCESS,
  payload,
});

const getTaskError = payload => ({
  type: Constant.TASK_DATA_ERROR,
  payload,
});

export {loadTaskData, getTaskError, getTaskResponse};
