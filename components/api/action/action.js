import {Constant} from '../../utils/Constant';
// COUNTRY API
const loadCountryData = () => ({
  type: Constant.COUNTRY_DATA,
});

const getResponse = payload => ({
  type: Constant.DATA_SUCCESS,
  payload,
});

const getError = payload => ({
  type: Constant.DATA_ERROR,
  payload,
});

export {loadCountryData, getError, getResponse};
