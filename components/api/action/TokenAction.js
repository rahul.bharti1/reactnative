import {Constant} from '../../utils/Constant';
// TOKEN API
const loadTokenData = (user_id, access_token, mType) => ({
  type: Constant.TOKEN_DATA,
  user_id,
  access_token,
  mType,
});

const getTokenResponse = payload => ({
  type: Constant.TOKEN_DATA_SUCCESS,
  payload,
});

const getTokenError = payload => ({
  type: Constant.TOKEN_DATA_ERROR,
  payload,
});

export {loadTokenData, getTokenError, getTokenResponse};
