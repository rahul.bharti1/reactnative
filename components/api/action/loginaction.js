import {Constant} from '../../utils/Constant';
// LoginData  API
const loadLoginData = (username, password) => ({
  type: Constant.LOGIN_DATA,
  username,
  password,
});

const getLoginResponse = payload => ({
  type: Constant.LOGIN_DATA_SUCCESS,
  payload,
});

const getLoginError = payload => ({
  type: Constant.LOGIN_DATA_ERROR,
  payload,
});

export {loadLoginData, getLoginError, getLoginResponse};
