/* eslint-disable semi */
/* eslint-disable prettier/prettier */

const BASE_URL = 'https://dev.kreatetechnologies.com/sari/api'

const endUrl = '/dashboard/getcountries';
const headers = {'Content-Type': 'application/json'};

// STEP 1
export const getCountries = async () => {
    console.log(BASE_URL + '' + endUrl)

    const response = await fetch(BASE_URL + endUrl, {
        method: 'GET',
    })

    const data = response.json()
    if (response.status > 400) {
        throw new Error(data.errors)
    }
    return data;
}

 export const getTask = async (org) => {
    console.log('http://dev02.kreatetechnologies.com:1080/saksham/' + '' + 'getprojectclearnces')
    let format = new FormData()
    format.append('org', org)
    const response = await fetch('http://dev02.kreatetechnologies.com:1080/saksham/' + 'getprojectclearnces', {
        method: 'POST',
        // headers: headers,
        body: format,
        // body: JSON.stringify({
        //     id: id
        // }),

    })

    const data = response.json()
    if (response.status > 400) {
        throw new Error(data.errors)
    }
    return data;
}


export const getLogin = async (username, password) => {
    console.log('https://dev.kreatetechnologies.com/nwr/app/api/app/checklogin')
    let format = new FormData()
    format.append('email', username)
    format.append('password',password)
    const response = await fetch('https://dev.kreatetechnologies.com/nwr/app/api/app/checklogin', {
        method: 'POST',
        headers: headers,
        // body: format,
        body: JSON.stringify({
            email: username,
            password:password,
        }),
    })

    const data = response.json()
    if (response.status > 400) {
        throw new Error(data.errors)
    }
    return data;
}


export const getToken = async (user_id, access_token, mType) => {
    console.log('https://dev.kreatetechnologies.com/nwr/app/api/app/generatetoken')
    let format = new FormData()
    let strBody = JSON.stringify({
        user_id: user_id,
        access_token:access_token,
        type: mType})
        console.log('getToken json request = ' + strBody)

    format.append('user_id', user_id)
    format.append('access_token',access_token)
    const response = await fetch('https://dev.kreatetechnologies.com/nwr/app/api/app/generatetoken', {
        method: 'POST',
        headers: headers,
        // body: format,
        body: strBody,
    })

    const data = response.json()
    if (response.status > 400) {
        throw new Error(data.errors)
    }
    return data;
}

