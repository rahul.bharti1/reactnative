/* eslint-disable eol-last */
/* eslint-disable semi */
/* eslint-disable eqeqeq */
/* eslint-disable prettier/prettier */
import {Constant} from '../../utils/Constant'

export const taskLoginReducer = (state = '', action) => {
    if (action.type == Constant.LOGIN_DATA_SUCCESS) {
        return action.payload;
    }
    if (action.type == Constant.LOGIN_DATA_ERROR) {
        return action.payload
    }
    return state;
}