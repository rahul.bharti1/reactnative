/* eslint-disable eol-last */
/* eslint-disable semi */
/* eslint-disable eqeqeq */
/* eslint-disable prettier/prettier */
// import {taskDataReducer} from '../../api/reducer/TaskReducer'
import {taskLoginReducer} from '../../api/reducer/loginReducer'
import {taskTokenReducer} from '../../api/reducer/TokenReducer'

// import {countryDataReducer} from '../../api/reducer/CountryReducer'
import { combineReducers } from 'redux'

const rootReducers = combineReducers({
    // countryDataResponse: countryDataReducer,
    // taskResponse: taskDataReducer,
    loginResponse: taskLoginReducer,
    tokenResponse: taskTokenReducer,
})

export default rootReducers;