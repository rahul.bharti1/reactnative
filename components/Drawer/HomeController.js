/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import GenerateToken from '../ui/GenerateToken';
import CustomNavigation from '../ui/CustomNavigation';


import Task from '../ui/Task';
const Drawer = createDrawerNavigator();

class HomeController extends Component {
    constructor() {
      super();
    }

    render(){
        return (
          <Drawer.Navigator initialRouteName="GenerateToken" drawerContent={(props) => <CustomNavigation {...props} />}>
          {/* <Drawer.Screen name="MyList" component={MyList} options={{
            activeTintColor: '#e91e63', itemStyle: {marginVertical: 5},drawerLabel: 'Home',
}}/> */}
          <Drawer.Screen name="GenerateToken" component={GenerateToken} />
        </Drawer.Navigator>
        );
    }
}

// function NotificationsScreen({ navigation }) {
//   return (
//    <SignUp />
//   );
// }
export default HomeController;
