/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  Platform,
  StyleSheet,
  ImageBackground,
  Button,
} from 'react-native';
import {Provider} from 'react-redux';
import Login from './components/ui/login/Login';

import Splash from './components/ui/Splash';

import MyList from './components/ui/MyList';
import GenerateToken from './components/ui/GenerateToken';
import Token from './components/ui/Token';



import HomeController from './components/Drawer/HomeController';
import configurestore from './components/api/store/store';

const url = {uri: 'https://reactjs.org/logo-og.png'};
const store = configurestore();

// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       name: Constant.name,
//     };
//   }
//   render() {
//     console.log(Platform.OS);
//     return (
//      // eslint-disable-next-line-react-native/no-inline-styles
//       <SafeAreaView style={{flex: 1}}>
//         <ImageBackground source={url} style={{flex: 1}}>
//           <Provider store={store}>
//             <Login username={this.state.name} />
//           </Provider>
//         </ImageBackground>
//       </SafeAreaView>
//       // <Navigation />
//     );
//   }
// }

// eslint-disable-next-line no-lone-blocks
{/* <MyList store={store} /> */}
import Survey from './components/ui/survey';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

const App = () => {
  // eslint-disable-next-line no-return-assign
  return (
    <SafeAreaView style={{flex: 1}}>
    <Provider store={store}>
    <NavigationContainer>
    <Stack.Navigator initialRouteName={'Splash'}>
    <Stack.Screen name="Splash" component={Splash} options={{ title: 'Splash', headerShown: false }}  />
      <Stack.Screen name="Login" component={Login} options={{ title: 'Login', headerShown: false }}  />
      <Stack.Screen name="HomeController" component={HomeController} options={{ title: 'Back', headerShown: false }} />
      <Stack.Screen name="Token" component={Token} options={{ title: 'Token', headerShown: true }}  />
      <Stack.Screen name="GenerateToken" component={GenerateToken} options={{ title: 'GenerateToken', headerShown: false }}  />

    </Stack.Navigator>
  </NavigationContainer>
  </Provider>
  </SafeAreaView>
  );
};

export default App;
